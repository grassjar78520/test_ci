FROM node:12

WORKDIR /app
ADD hello.js package.json ./

RUN npm install
CMD node hello.js